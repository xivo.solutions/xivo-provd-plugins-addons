# -*- coding: utf-8 -*-

# Copyright (C) 2014-2016 Avencall
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>

common_globals = {}
execfile_('common.py', common_globals)

MODELS = {
    u'VVX101': u'5.4.3',
    u'VVX201': u'5.4.3',
    u'VVX300': u'5.4.3',
    u'VVX310': u'5.4.3',
    u'VVX400': u'5.4.3',
    u'VVX410': u'5.4.3',
    u'VVX500': u'5.4.3',
    u'VVX600': u'5.4.3',
    u'VVX1500': u'5.4.3',
}


class PolycomPlugin(common_globals['BasePolycomPlugin']):
    IS_PLUGIN = True

    pg_associator = common_globals['BasePolycomPgAssociator'](MODELS)
