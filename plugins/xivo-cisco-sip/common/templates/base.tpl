<device>
  <fullConfig>true</fullConfig>
  <deviceProtocol>SIP</deviceProtocol>
  <ipAddressMode>0</ipAddressMode>
  <allowAutoConfig>true</allowAutoConfig>
  <dadEnable>true</dadEnable>
  <redirectEnable>false</redirectEnable>
  <echoMultiEnable>false</echoMultiEnable>
  <ipPreferenceModeControl>0</ipPreferenceModeControl>
  <ipMediaAddressFamilyPreference>0</ipMediaAddressFamilyPreference>
  <devicePool>
    <dateTimeSetting>
      <dateTemplate>D/M/Y</dateTemplate>
      <timeZone>Central Europe Standard/Daylight Time</timeZone>
      <ntps>
        {% if ntp_enabled -%}
        <ntp>
          <name>{{ ntp_ip }}</name>
          <ntpMode>Unicast</ntpMode>
        </ntp>
        {% endif -%}
      </ntps>
    </dateTimeSetting>
    <callManagerGroup>
      <members>
        <member priority="0">
          <callManager>
            <ports>
              <sipPort>{{ sip_proxy_port }}</sipPort>
              <securedSipPort>5061</securedSipPort>
            </ports>
            <processNodeName>{{ sip_proxy_ip }}</processNodeName>
          </callManager>
        </member>
      {% if sip_backup_proxy_ip -%}
        <member priority="1">
          <callManager>
            <ports>
              <sipPort>{{ sip_backup_proxy_port }}</sipPort>
              <securedSipPort>5061</securedSipPort>
            </ports>
            <processNodeName>{{ sip_backup_proxy_ip }}</processNodeName>
          </callManager>
        </member>
      {% endif -%}
      </members>
    </callManagerGroup>
  </devicePool>
  <!-- <TVS>
    <members>
      <member priority="0">
        <port>2445</port>
        <address></address>
      </member>
    </members>
  </TVS> -->
  <!-- <vpnGroup>
    <mtu>1290</mtu>
    <failConnectTime>30</failConnectTime>
    <authMethod>0</authMethod>
    <pswdPersistent>1</pswdPersistent>
    <autoNetDetect>1</autoNetDetect>
    <enableHostIDCheck>0</enableHostIDCheck>
    <addresses>
      <url1></url1>
    </addresses>
    <credentials>
      <hashAlg>0</hashAlg>
      <certHash1></certHash1>
    </credentials>
  </vpnGroup> -->
  <sipProfile>
    <sipProxies>
      <outboundProxy>{{ sip_outbound_proxy_ip }}</outboundProxy>
      <outboundProxyPort>{{ sip_outbound_proxy_port }}</outboundProxyPort>
      <registerWithProxy>true</registerWithProxy>
    </sipProxies>
    <sipCallFeatures>
      <cnfJoinEnabled>true</cnfJoinEnabled>
      <callForwardURI>x-cisco-serviceuri-cfwdall</callForwardURI>
      <callPickupURI>x-cisco-serviceuri-pickup</callPickupURI>
      <callPickupListURI>x-cisco-serviceuri-opickup</callPickupListURI>
      <callPickupGroupURI>x-cisco-serviceuri-gpickup</callPickupGroupURI>
      <meetMeServiceURI>x-cisco-serviceuri-meetme</meetMeServiceURI>
      <abbreviatedDialURI>x-cisco-serviceuri-abbrdial</abbreviatedDialURI>
      <rfc2543Hold>false</rfc2543Hold>
      <callHoldRingback>1</callHoldRingback>
      <localCfwdEnable>true</localCfwdEnable>
      <semiAttendedTransfer>true</semiAttendedTransfer>
      <anonymousCallBlock>0</anonymousCallBlock>
      <callerIdBlocking>0</callerIdBlocking>
      <dndControl>0</dndControl>
      <remoteCcEnable>true</remoteCcEnable>
      <retainForwardInformation>false</retainForwardInformation>
      <uriDialingDisplayPreference>1</uriDialingDisplayPreference>
    </sipCallFeatures>
    <sipStack>
      <sipInviteRetx>6</sipInviteRetx>
      <sipRetx>10</sipRetx>
      <timerInviteExpires>180</timerInviteExpires>
      <timerRegisterExpires>3600</timerRegisterExpires>
      <timerRegisterDelta>5</timerRegisterDelta>
      <timerKeepAliveExpires>120</timerKeepAliveExpires>
      <timerSubscribeExpires>120</timerSubscribeExpires>
      <timerSubscribeDelta>5</timerSubscribeDelta>
      <timerT1>500</timerT1>
      <timerT2>4000</timerT2>
      <maxRedirects>70</maxRedirects>
      <remotePartyID>true</remotePartyID>
      <userInfo>Phone</userInfo>
    </sipStack>
    <autoAnswerTimer>1</autoAnswerTimer>
    <autoAnswerAltBehavior>false</autoAnswerAltBehavior>
    <autoAnswerOverride>true</autoAnswerOverride>
    <transferOnhookEnabled>true</transferOnhookEnabled>
    <enableVad>false</enableVad>
    <preferredCodec>none</preferredCodec>
    <dtmfAvtPayload>101</dtmfAvtPayload>
    <dtmfDbLevel>3</dtmfDbLevel>
    <dtmfOutofBand>avt</dtmfOutofBand>
    <alwaysUsePrimeLine>false</alwaysUsePrimeLine>
    <alwaysUsePrimeLineVoiceMail>false</alwaysUsePrimeLineVoiceMail>
    <kpml>0</kpml>
    <phoneLabel>{% if '1' in sip_lines %}{{ sip_lines['1']['number'] }}{% endif %}</phoneLabel>
    <stutterMsgWaiting>0</stutterMsgWaiting>
    <callStats>true</callStats>
    <offhookToFirstDigitTimer>15000</offhookToFirstDigitTimer>
    <silentPeriodBetweenCallWaitingBursts>10</silentPeriodBetweenCallWaitingBursts>
    <disableLocalSpeedDialConfig>false</disableLocalSpeedDialConfig>
    <startMediaPort>16384</startMediaPort>
    <stopMediaPort>32766</stopMediaPort>
    <natEnabled>false</natEnabled>
    <natReceivedProcessing>false</natReceivedProcessing>
    <natAddress></natAddress>
    <sipLines>
    {% for line_no, line in sip_lines.items() -%}
      <line button="{{ line_no }}" lineIndex="{{ line_no }}">
        <featureID>9</featureID>
        <featureLabel>{{ line['number'] }}</featureLabel>
        <proxy>USECALLMANAGER</proxy>
        <port>{{ line['proxy_port']|d(5060) }}</port>
        <name>{{ line['username'] }}</name>
        <displayName>{{ line['display_name'] }}</displayName>
        <autoAnswer>
          <autoAnswerEnabled>0</autoAnswerEnabled>
        </autoAnswer>
        <callWaiting>3</callWaiting>
        <authName>{{ line['auth_username'] }}</authName>
        <authPassword>{{ line['password'] }}</authPassword>
        <contact></contact>
        <sharedLine>false</sharedLine>
        <messageWaitingLampPolicy>3</messageWaitingLampPolicy>
        <messageWaitingAMWI>0</messageWaitingAMWI>
        {% if line['voicemail'] -%}
        <messagesNumber>{{ line['voicemail'] }}</messagesNumber>
        {% endif -%}
        <ringSettingIdle>4</ringSettingIdle>
        <ringSettingActive>5</ringSettingActive>
        <forwardCallInfoDisplay>
          <callerName>true</callerName>
          <callerNumber>true</callerNumber>
          <redirectedNumber>false</redirectedNumber>
          <dialedNumber>true</dialedNumber>
        </forwardCallInfoDisplay>
        <maxNumCalls>5</maxNumCalls>
        <busyTrigger>4</busyTrigger>
      </line>
    {% endfor -%}
    </sipLines>
    {% if '1' in sip_lines -%}
    <externalNumberMask>{{ sip_lines['1']['number'] }}</externalNumberMask>
    <voipControlPort>{{ sip_lines['1']['proxy_port']|d(5060) }}</voipControlPort>
    {% endif -%}
    <dscpForAudio>184</dscpForAudio>
    <ringSettingBusyStationPolicy>0</ringSettingBusyStationPolicy>
    <dialTemplate>dialplan.xml</dialTemplate>
    <softKeyFile>SoftKeys.xml</softKeyFile>
  </sipProfile>
  <MissedCallLoggingOption>1</MissedCallLoggingOption>
  <featurePolicyFile></featurePolicyFile>
  <commonProfile>
    <phonePassword>{{ admin_password|d('9486') }}</phonePassword>
    <backgroundImageAccess>true</backgroundImageAccess>
    <callLogBlfEnabled>2</callLogBlfEnabled>
  </commonProfile>
  <vendorConfig>
    <defaultWallpaperFile></defaultWallpaperFile>
    <disableSpeaker>false</disableSpeaker>
    <disableSpeakerAndHeadset>false</disableSpeakerAndHeadset>
    <enableMuteFeature>false</enableMuteFeature>
    <enableGroupListen>true</enableGroupListen>
    <holdResumeKey>1</holdResumeKey>
    <recentsSoftKey>1</recentsSoftKey>
    <dfBit>1</dfBit>
    <pcPort>0</pcPort>
    <spanToPCPort>1</spanToPCPort>
    <garp>0</garp>
    <rtcp>1</rtcp>
    <videoRtcp>1</videoRtcp>
    {% if vlan_enabled -%}
    <voiceVlanAccess>{{ vlan_id }}</voiceVlanAccess>
    {% endif -%}
    <videoCapability>1</videoCapability>
    <hideVideoByDefault>0</hideVideoByDefault>
    <separateMute>0</separateMute>
    <ciscoCamera>1</ciscoCamera>
    <usb1>1</usb1>
    <usb2>1</usb2>
    <usbClasses>0,1,2</usbClasses>
    <sdio>1</sdio>
    <wifi>1</wifi>
    <bluetooth>1</bluetooth>
    <bluetoothProfile>0,1</bluetoothProfile>
    <btpbap>0</btpbap>
    <bthfu>0</bthfu>
    <ehookEnable>0</ehookEnable>
    <autoSelectLineEnable>1</autoSelectLineEnable>
    <autoCallSelect>1</autoCallSelect>
    <incomingCallToastTimer>10</incomingCallToastTimer>
    <dialToneFromReleaseKey>0</dialToneFromReleaseKey>
    <joinAndDirectTransferPolicy>0</joinAndDirectTransferPolicy>
    <minimumRingVolume></minimumRingVolume>
    <simplifiedNewCall>0</simplifiedNewCall>
    <actionableAlert>0</actionableAlert>
    <showCallHistoryForSelectedLine>0</showCallHistoryForSelectedLine>
    <kemOneColumn>0</kemOneColumn>
    <lineMode>0</lineMode>
    <allCallsOnPrimary>0</allCallsOnPrimary>
    <softKeyControl>0</softKeyControl>
    <settingsAccess>1</settingsAccess>
    <webProtocol>0</webProtocol>
    <webAccess>0</webAccess>
    <webAdmin>1</webAdmin>
    <adminPassword></adminPassword>
    <sshAccess>0</sshAccess>
    <detectCMConnectionFailure>0</detectCMConnectionFailure>
    <g722CodecSupport>1</g722CodecSupport>
    <handsetWidebandEnable>2</handsetWidebandEnable>
    <headsetWidebandEnable>2</headsetWidebandEnable>
    <headsetWidebandUIControl>1</headsetWidebandUIControl>
    <handsetWidebandUIControl>1</handsetWidebandUIControl>
    <daysDisplayNotActive>1,7</daysDisplayNotActive>
    <displayOnTime>08:00</displayOnTime>
    <displayOnDuration>10:00</displayOnDuration>
    <displayIdleTimeout>00:10</displayIdleTimeout>
    <displayOnWhenIncomingCall>1</displayOnWhenIncomingCall>
    <displayRefreshRate>0</displayRefreshRate>
    <daysBacklightNotActive>1,7</daysBacklightNotActive>
    <backlightOnTime>08:00</backlightOnTime>
    <backlightOnDuration>10:00</backlightOnDuration>
    <backlightIdleTimeout>00:10</backlightIdleTimeout>
    <backlightOnWhenIncomingCall>1</backlightOnWhenIncomingCall>
    <recordingTone>0</recordingTone>
    <recordingToneLocalVolume>100</recordingToneLocalVolume>
    <recordingToneRemoteVolume>50</recordingToneRemoteVolume>
    <recordingToneDuration></recordingToneDuration>
    <moreKeyReversionTimer>5</moreKeyReversionTimer>
    <peerFirmwareSharing>0</peerFirmwareSharing>
    <loadServer></loadServer>
    <problemReportUploadURL></problemReportUploadURL>
    <enableCdpSwPort>1</enableCdpSwPort>
    <enableCdpPcPort>0</enableCdpPcPort>
    <enableLldpSwPort>1</enableLldpSwPort>
    <enableLldpPcPort>0</enableLldpPcPort>
    <cdpEnable>true</cdpEnable>
    <powerNegotiation>0</powerNegotiation>
    <outOfRangeAlert>0</outOfRangeAlert>
    <scanningMode>2</scanningMode>
    <applicationURL></applicationURL>
    <appButtonTimer>0</appButtonTimer>
    <appButtonPriority>0</appButtonPriority>
    <specialNumbers></specialNumbers>
    <sendKeyAction>0</sendKeyAction>
    <powerOffWhenCharging>0</powerOffWhenCharging>
    <homeScreen>0</homeScreen>
    <accessContacts>1</accessContacts>
    <accessFavorites>1</accessFavorites>
    <accessVoicemail>1</accessVoicemail>
    <accessApps>1</accessApps>
  </vendorConfig>
  <versionStamp>d902ed5a-c1e5-4233-b1d6-a960d53d1c3a</versionStamp>
  <loadInformation>{% block loadInformation %}{% endblock %}</loadInformation>
  <inactiveLoadInformation></inactiveLoadInformation>
  <!-- <addOnModules>
    <addOnModule idx="1">
      <deviceType></deviceType>
      <deviceLine></deviceLine>
      <loadInformation></loadInformation>
    </addOnModule>
  </addOnModules> -->
  <phoneServices useHTTPS="false">
    <provisioning>2</provisioning>
    <phoneService type="1" category="0">
      <name>Missed Calls</name>
      <url>Application:Cisco/MissedCalls</url>
      <vendor></vendor>
      <version></version>
    </phoneService>
    <phoneService type="1" category="0">
      <name>Received Calls</name>
      <url>Application:Cisco/ReceivedCalls</url>
      <vendor></vendor>
      <version></version>
    </phoneService>
    <phoneService type="1" category="0">
      <name>Placed Calls</name>
      <url>Application:Cisco/PlacedCalls</url>
      <vendor></vendor>
      <version></version>
    </phoneService>
    <phoneService type="2" category="0">
      <name>Voicemail</name>
      <url>Application:Cisco/Voicemail</url>
      <vendor></vendor>
      <version></version>
    </phoneService>
  </phoneServices>
  <userLocale>
    <name></name>
    <uid>1</uid>
    <langCode></langCode>
    <version></version>
    <winCharSet>utf-8</winCharSet>
  </userLocale>
  <networkLocale></networkLocale>
  <networkLocaleInfo>
    <name></name>
    <version></version>
  </networkLocaleInfo>
  <deviceSecurityMode>1</deviceSecurityMode>
  <idleTimeout>0</idleTimeout>
  <authenticationURL></authenticationURL>
  <messagesURL></messagesURL>
  <servicesURL></servicesURL>
  <directoryURL>{{ XX_xivo_phonebook_url|e }}&amp;</directoryURL>
  <idleURL></idleURL>
  <informationURL></informationURL>
  <proxyServerURL></proxyServerURL>
  <secureAuthenticationURL></secureAuthenticationURL>
  <secureMessagesURL></secureMessagesURL>
  <secureServicesURL></secureServicesURL>
  <secureDirectoryURL></secureDirectoryURL>
  <secureInformationURL></secureInformationURL>
  <secureIdleURL></secureIdleURL>
  <transportLayerProtocol>1</transportLayerProtocol>
  <TLSResumptionTimer>3600</TLSResumptionTimer>
  <phonePersonalization>1</phonePersonalization>
  <autoCallPickupEnable>true</autoCallPickupEnable>
  <blfAudibleAlertSettingOfIdleStation>0</blfAudibleAlertSettingOfIdleStation>
  <blfAudibleAlertSettingOfBusyStation>0</blfAudibleAlertSettingOfBusyStation>
  <dndCallAlert>1</dndCallAlert>
  <dndReminderTimer>5</dndReminderTimer>
  <advertiseG722Codec>1</advertiseG722Codec>
  <rollover>0</rollover>
  <joinAcrossLines>0</joinAcrossLines>
  <capfAuthMode>0</capfAuthMode>
  <!-- <capfList>
    <capf>
      <phonePort>3804</phonePort>
      <processNodeName></processNodeName>
    </capf>
  </capfList> -->
  <certHash></certHash>
  <encrConfig>false</encrConfig>
  <userId></userId>
  <ownerId></ownerId>
  <sshUserId>admin</sshUserId>
  <sshPassword>{{ admin_password|d('9486') }}</sshPassword>
</device>
