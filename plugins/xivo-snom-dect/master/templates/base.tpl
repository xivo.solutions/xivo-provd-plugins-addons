<?xml version="1.0" encoding="UTF-8" ?>
<settings>
  <phone-settings>
    {% if vlan_enabled -%}
    <vlan_id perm="R">{{ vlan_id }}</vlan_id>
    <vlan_qos perm="R">{{ vlan_priority|d('0') }}</vlan_qos>
    {% else -%}
    <vlan_id perm="R"></vlan_id>
    <vlan_qos perm="R"></vlan_qos>
    {% endif -%}

    <network_vlan_synchronization>0</network_vlan_synchronization>

    <signaling_tos perm="R">104</signaling_tos>
    <codec_tos perm="R">184</codec_tos>

    <http_user perm="R">{{ admin_username|d('admin') }}</http_user>
    <http_pass perm="R">{{ admin_password|d('Administrator') }}</http_pass>
    <http_basic_user perm="R">User</http_basic_user>
    <http_basic_pass perm="R">{{ user_password|d('Userpassword') }}</http_basic_pass>
    <http_engineer_user perm="R">Engineer</http_engineer_user>
    <http_engineer_pass perm="R">{{ admin_password|d('Engineerpassword') }}</http_engineer_pass>
    <ac_code>9486</ac_code>

    {% if ntp_enabled -%}
    <ntp_server perm="R">{{ ntp_ip }}</ntp_server>
    {% else -%}
    <ntp_server perm="R"></ntp_server>
    {% endif -%}

    <server>
    {% for ip_mds in mds -%}
    <user_dtmf_info idx="{{ mds.index(ip_mds) + 1 }}">sip_info_only</user_dtmf_info>
    <alert_info_playback idx="{{ mds.index(ip_mds) + 1 }}">on</alert_info_playback>
    <srv_sip_server_alias idx="{{ mds.index(ip_mds) + 1 }}">server_{{ mds.index(ip_mds) + 1 }}</srv_sip_server_alias>
    <codec_priority_list idx="{{ mds.index(ip_mds) + 1 }}">pcma, g722, pcmu, g726</codec_priority_list>
    <user_host idx="{{ mds.index(ip_mds) + 1 }}">{{ ip_mds }}</user_host>
    <user_outbound idx="{{ mds.index(ip_mds) + 1 }}">{{ ip_mds }}</user_outbound>
    {% endfor -%}
    </server>

    <extension>
    {% for line_no, line in sip_lines.items() -%}
    <subscr_dect_ipui idx="{{ line_no }}">{{ line['ipei'] }}</subscr_dect_ipui>
    <subscr_sip_hs_idx idx="{{ line_no }}">{{ line_no }}</subscr_sip_hs_idx>
    <subscr_sip_line_name idx="{{ line_no }}">{{ line['number'] }}</subscr_sip_line_name>
    <user_pname idx="{{ line_no }}">{{ line['auth_username']|e }}</user_pname>
    <user_pass idx="{{ line_no }}">{{ line['password']|e }}</user_pass>
    <user_active idx="{{ line_no }}">on</user_active>
    <subscr_sip_ua_data_server_id idx="{{ line_no }}">{{ line['mds_id'] }}</subscr_sip_ua_data_server_id>
    <user_name idx="{{ line_no }}">{{ line['auth_username']|e }}</user_name>
    <user_mailbox idx="{{ line_no }}">{{ line['display_name']|e }}</user_mailbox>
    <user_mailnumber idx="{{ line_no }}">{{ line['voicemail'] }}</user_mailnumber>
    <user_realname idx="{{ line_no }}">{{ line['display_name']|e }}</user_realname>
    {% endfor -%}
    </extension>

    <multicell>
    <network_allow_multi_primary>off</network_allow_multi_primary>
    <network_auto_multi_primary>off</network_auto_multi_primary>
    <network_dect_auto_sync_tree_config>on</network_dect_auto_sync_tree_config>
    <network_roaming_deregister>off</network_roaming_deregister>
    <network_sync_chain_id>512</network_sync_chain_id>
    <network_sync_data_transport>multicast</network_sync_data_transport>
    <network_sync_debug_enable>off</network_sync_debug_enable>
    <network_sync_enable>on</network_sync_enable>
    <network_sync_max_sip_reg_per_base>8</network_sync_max_sip_reg_per_base>
    <network_sync_primary_static_ip>{{ ip }}</network_sync_primary_static_ip>
    <network_sync_time>60</network_sync_time>
    </multicell>

    {% if XX_lang -%}
    <language perm="R">{{ XX_lang[0] }}</language>
    <web_language perm="R">{{ XX_lang[0] }}</web_language>
    <tone_scheme perm="R">{{ XX_lang[1] }}</tone_scheme>
    {% endif -%}

    {{ XX_timezone }}
  </phone-settings>
</settings>
