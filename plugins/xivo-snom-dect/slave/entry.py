# -*- coding: utf-8 -*-

# Copyright (C) 2014-2019 Avencall
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>

common_globals = {}
execfile_('common.py', common_globals)

MODELS = [
    u'M700',
    u'M900',
]
VERSION = [
    u'04.10.0014',
    u'04.50.0008',
    u'04.50.0011',
    u'04.50.0013',
    u'05.00.0001',
    u'05.01.0001',
    u'05.10.0001',
    u'05.10.0002',
    u'05.20.0001',
    u'05.30.0002',
    u'05.30.0007',
    u'06.10.0003',
    u'06.10.0005',
    u'06.50.0002',
]

class SnomBornesPlugin(common_globals['BaseSnomBornesPlugin']):
    IS_PLUGIN = True

    _MODELS = MODELS

    _SYNC_NOTIFY_EVENT='check-sync;reboot=true'

    pg_associator = common_globals['BaseSnomBornesPgAssociator'](MODELS, VERSION)
