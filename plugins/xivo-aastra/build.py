# -*- coding: utf-8 -*-

# Copyright (C) 2014 Avencall
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>

# Depends on the following external programs:
#  -rsync

from subprocess import check_call


@target('67XX', 'xivo-aastra-67XX')
def build_67XX(path):
    check_call(['rsync', '-rlp', '--exclude', '.*',
                '--exclude', '/templates/68*.tpl',
                'common/', path])

    check_call(['rsync', '-rlp', '--exclude', '.*',
                '67XX/', path])


@target('68XX', 'xivo-aastra-68XX')
def build_68XX(path):
    check_call(['rsync', '-rlp', '--exclude', '.*',
                '--exclude', '/templates/67*',
                'common/', path])

    check_call(['rsync', '-rlp', '--exclude', '.*',
                '68XX/', path])

# @target('3.3.1-SP4-HF9', 'xivo-aastra-3.3.1-SP4-HF9')
# def build_3_3_1_sp4_hf9(path):
#     check_call(['rsync', '-rlp', '--exclude', '.*',
#                 '--exclude', '/templates/68*.tpl',
#                 'common/', path])

#     check_call(['rsync', '-rlp', '--exclude', '.*',
#                 '3.3.1-SP4-HF9/', path])


# @target('4.3.0', 'xivo-aastra-4.3.0')
# def build_4_3_0(path):
#     check_call(['rsync', '-rlp', '--exclude', '.*',
#                 '--exclude', '/templates/67*',
#                 'common/', path])

#     check_call(['rsync', '-rlp', '--exclude', '.*',
#                 '4.3.0/', path])
